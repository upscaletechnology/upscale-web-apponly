import { applyMiddleware, createStore } from 'redux';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';

import rootReducer from './reducers/rootReducer';

const logger = createLogger({
  predicate: (getState, action) => !action.type.includes('@@redux-form')
  // ...options
});

// create a middleware for rev or prod
const ENVmiddleware = () => {
  if (process.env.NODE_ENV === 'production') {
    return applyMiddleware(thunk);
  }
  return applyMiddleware(thunk, logger);
  // return applyMiddleware(thunk);
};


const middleware = ENVmiddleware();

const configureStore = () => createStore(
  rootReducer,
  middleware
);

export default configureStore;
