const path = {
  HOMEPAGE: '/',
  JOBS: '/Jobs',
  PRESS: '/Press',
  CONTACT: '/Contact',
  ABOUT: '/About',
  BUYNOW: '/BuyNow',
  BLOG: '/Blog',
  APPSTORE: '/AppStore',
  PLAYSTORE: '/PlayStore',


  LOGIN: '/Dashboard/Login',
  SIGNUP: '/Dashboard/Signup',
  DASHBOARD: '/Dashboard'
};

export default path;
