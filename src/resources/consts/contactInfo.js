const contactInfo = {
  PHONE: '514-717-2800',
  EMAIL: 'hi@upscale-technology.com',

  TWITTER: 'https://twitter.com/upscaletechno',
  FACEBOOK: 'https://www.facebook.com/upscaletechnology/',
  INSTAGRAM: 'https://www.instagram.com/upscaletechnology/',
  PLAYSTORE: 'https://play.google.com/store/apps/details?id=com.uptune',
  APPSTORE: 'https://itunes.apple.com/us/app/uptune/id1436738898?l=fr&ls=1&mt=8'
};

export default contactInfo;
