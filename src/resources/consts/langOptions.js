const langOptions = {
  FR: 'FR',
  EN: 'EN'
};

export default langOptions;
