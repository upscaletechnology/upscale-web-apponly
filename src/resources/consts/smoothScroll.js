const smoothScroll = {
  TOP: 'top',
  JUMBOTRON: 'jumbotron',
  CAROUSEL: 'carousel'
};

export default smoothScroll;
