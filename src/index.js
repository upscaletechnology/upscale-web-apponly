require('react-hot-loader/patch');
import React from 'react';
//import ReactDOM from 'react-dom';
import { render } from 'react-snapshot';
import configureStore from './store';
import Root from './containers/Root';
import { AppContainer } from 'react-hot-loader';
import * as firebase from 'firebase';
import { Helmet } from 'react-helmet';

import './styling/semantic.min.css';
import './styling/global.scss';

import favicon from './favicon.ico';
import metaImg from './timeWizard.jpg';

export const store = configureStore();

var config = {
  apiKey: 'AIzaSyBZvy2WlK2ampJY4xFiyAmfjzH5QfHlXi8',
  authDomain: 'upscale-website.firebaseapp.com',
  databaseURL: 'https://upscale-website.firebaseio.com',
  projectId: 'upscale-website',
  storageBucket: '',
  messagingSenderId: '765488510465'
};

firebase.initializeApp(config);

//ReactDOM.render(
render(
  <div>
    <Helmet>
      <link rel="icon" type="image/x-icon" href={favicon} />
      <meta charset="utf-8" />
      <meta
        name="description"
        content="Connectez deux téléphones mobiles ensemble : l'un écoutera et l'autre vous guidera dans vos soundchecks."
      />
      <meta name="image" content={metaImg} />
      <meta itemprop="name" content="Uptune : Devenez le Soundman" />
      <meta
        itemprop="description"
        content="Connectez deux téléphones mobiles ensemble : l'un écoutera et l'autre vous guidera dans vos soundchecks."
      />
      <meta itemprop="image" content={metaImg} />
      <meta name="og:title" content="Uptune : Devenez le Soundman" />
      <meta
        name="og:description"
        content="Connectez deux téléphones mobiles ensemble : l'un écoutera et l'autre vous guidera dans vos soundchecks."
      />
      <meta name="og:image" content={metaImg} />
      <meta name="og:url" content="https://upscale-candidate.ml" />
      <meta name="og:site_name" content="Upscale Technology" />
      <meta name="og:locale" content="fr_CA" />
      <meta name="fb:admins" content="341481649640043" />
      <meta name="og:type" content="website" />
    </Helmet>
    <AppContainer>
      <Root store={store} />
    </AppContainer>
  </div>,
  document.getElementById('root')
);
