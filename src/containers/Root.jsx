import React from 'react';
import { Provider } from 'react-redux';
import { Route, BrowserRouter, Switch } from 'react-router-dom';
import PropTypes from 'prop-types';

import path from '../resources/consts/path';
import HomePageRouter from './HomePageRouter';

const Root = ({ store }) => (
  <Provider store={store}>
    <BrowserRouter onUpdate={() => window.scrollTo(0, 0)}>
      <Switch>
        <Route path={path.HOMEPAGE} render={() => <HomePageRouter />} />
      </Switch>
    </BrowserRouter>
  </Provider>
);

Root.propTypes = {
  store: PropTypes.object.isRequired
};

export default Root;
