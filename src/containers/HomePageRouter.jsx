import React, { Component } from 'react';
import { Switch, Route, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ReactGA from 'react-ga';
import { Helmet } from 'react-helmet';

import path from '../resources/consts/path';

import Jumbotron from '../components/HomePage/Jumbotron/Jumbotron';
import About from '../components/HomePage/About/About';
import Contact from '../components/HomePage/Contact/Contact';
import Jobs from '../components/HomePage/Jobs/Jobs';
import AppNotAviable from '../components/HomePage/AppNotAviable/AppNotAviable';
import NotFound from '../components/HomePage/NotFound/NotFound';
import NavBar from '../components/HomePage/Navbar/Navbar';
import Footer from '../components/HomePage/Footer/Footer';

import text from './lang.json';

/*eslint-disable */
class HomePageRouter extends Component {
  componentDidMount() {
    ReactGA.initialize('UA-107970240-2');
    // console.log(window.location.pathname + window.location.search);
    ReactGA.pageview(window.location.pathname + window.location.search);
  }

  componentDidUpdate() {
    this.props.history.listen(location => {
      if (location.hash === '') {
        window.scrollTo(0, 0);
      }
    });
  }

  shouldComponentUpdate(nextProps) {
    if (this.props.location.pathname !== nextProps.location.pathname) {
      // console.log(window.location.pathname + window.location.search);
      ReactGA.pageview(window.location.pathname + window.location.search);
    }
    return true;
  }

  /* eslint-enable */
  render() {
    return (
      <div className='bm-page-wrapper'>
        <Helmet>
          <title>{text[this.props.lang].tabTitle}</title>
        </Helmet>
        <NavBar
          activeItem={this.props.menu.activeItem}
          toggle={this.props.menu.toggle}
          lang={this.props.lang}
        />
        <Switch>
          <Route
            exact
            path={path.HOMEPAGE}
            render={() => <Homepage lang={this.props.lang} />}
          />
          <Route
            path={path.ABOUT}
            render={() => <About lang={this.props.lang} />}
          />
          <Route
            path={path.CONTACT}
            render={() => <Contact lang={this.props.lang} />}
          />
          <Route
            path={path.JOBS}
            render={props => (
              <Jobs match={props.match} lang={this.props.lang} />
            )}
          />
          <Route
            path={path.APPSTORE}
            render={() => (
              <AppNotAviable
                mailchimp='https://upscale-technology.us17.list-manage.com/subscribe/post?u=9dee5e4da2c3fb6617f9ce97e&id=0b79aa1ffb'
                lang={this.props.lang}
              />
            )}
          />
          <Route
            render={() => (
              <AppNotAviable
                mailchimp='https://upscale-technology.us17.list-manage.com/subscribe/post?u=9dee5e4da2c3fb6617f9ce97e&amp;id=b346e9a2f3'
                lang={this.props.lang}
              />
            )}
          />
          <Route render={() => <NotFound lang={this.props.lang} />} />
        </Switch>
        <Footer lang={this.props.lang} />
      </div>
    );
  }
}

const Homepage = props => (
  <div>
    <Jumbotron lang={props.lang} />
  </div>
);

Homepage.propTypes = {
  lang: PropTypes.string.isRequired
};

HomePageRouter.propTypes = {
  menu: PropTypes.object.isRequired,
  lang: PropTypes.string.isRequired
};

const mapStateToProps = state => ({
  menu: state.menu,
  lang: state.lang
});

export default withRouter(connect(mapStateToProps)(HomePageRouter));
