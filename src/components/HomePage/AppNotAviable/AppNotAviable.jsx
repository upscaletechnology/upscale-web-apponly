import React from 'react';
import { Grid, Header, Container } from 'semantic-ui-react';
import { reduxForm, reset } from 'redux-form'; // Add Field when will do form validation

import text from './lang.json';

import './style.scss';

const AppNotAviable = props => (
  <div className='app-dev'>
    <Container>
      <Grid centered stackable>
        <Grid.Column width={7}>
          <Container>
            <Header as='h2'>{text[props.lang].header}</Header>
            <Header as='h3'>{text[props.lang].subheader}</Header>
          </Container>

          <form
            className='ui form'
            action={props.mailchimp}
            target='_blank'
            onSubmit='u50630(600,600)'
            id='u50630mailchimp'
            method='post'
          >
            <div className='field'>
              <label htmlFor='input01' className='form-labels'>
                {text[props.lang].email}
              </label>
              <input id='input01' type='email' name='EMAIL' />
            </div>

            <div className='field'>
              <label htmlFor='input02' className='form-labels'>
                {text[props.lang].iam}
              </label>
              <select name='DROPDOWN' id='input02'>
                <option value='Musicien'>
                  {text[props.lang].iamOptions[0]}
                </option>
                <option value='Technicien de son'>
                  {text[props.lang].iamOptions[1]}
                </option>
                <option value='Juste curieux'>
                  {text[props.lang].iamOptions[2]}
                </option>
              </select>
            </div>

            <div className='field'>
              <div className='button-wrap'>
                <button type='submit' name='subscribe' className='ui button'>
                  {text[props.lang].reservebtn}
                </button>
              </div>
            </div>
          </form>
        </Grid.Column>
      </Grid>
    </Container>
  </div>
);

const afterSubmit = (result, dispatch) => {
  dispatch(reset('AppForm'));
};

export default reduxForm({
  form: 'AppForm',
  onSubmitSuccess: afterSubmit
})(AppNotAviable);
