import React from 'react';
import { Grid, Header, Container, Image } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import contactInfo from './../../../resources/consts/contactInfo';
import buttonIPhone from './images/buttonIphone.png';
import buttonAndroid from './images/buttonAndroid.png';

import './style.scss';
import text from './lang.json';

const Jumbotron = props => (
  <Container className='jumbotron'>
    <div className='videoWrapper' >
      <iframe width='100%' height='100%' src='https://www.youtube.com/embed/TQCidmRs02A' frameBorder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowFullScreen />
    </div>
    <Header as='h1'>{text[props.lang].header}</Header>
    <div className='jumbotron-text'>{text[props.lang].subheader}</div>
    <div className='links-row'>
      <div className='apps-wrapper'>
        <a
          href='https://app.upscale-technology.com'
          target='_blank'
        >
          <div
            className='app-link'
          >
            <div style={{ top: '20px' }} className='link-icon'>
              <i className='code alternate icon fa-sm' />
            </div>
            <p>
              Web
            </p>
          </div>
        </a>
      </div>

      <div className='apps-wrapper'>
        <a
          href={contactInfo.APPSTORE}
          target='_blank'
        >
          <div
            className='app-link'
          >      <div className='link-icon'>
            <i className='apple alternate icon' />
          </div>
            <p style={{ width: '45%', textAlign: 'center' }}> iPhone</p>
          </div>
        </a>
      </div>

      <div className='apps-wrapper'>
        <a
          href={contactInfo.PLAYSTORE}
          target='_blank'
        >
          <div
            className='app-link'
          >      <div className='link-icon'>
            <i className='android alternate icon' />
          </div>
            <p style={{ width: '45%', textAlign: 'center' }}>Android</p>
          </div>
        </a>
      </div>
    </div>
    <div style={{ height: '136px' }} />
  </Container>
);

Jumbotron.propTypes = {
  lang: PropTypes.string.isRequired
};

export default Jumbotron;
