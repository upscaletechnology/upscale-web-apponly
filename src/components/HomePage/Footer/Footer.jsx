import React from 'react';
import { Grid, Header, Image, Dropdown, Flag } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import changeLang from '../../../actions/changeLang.action';

import contactInfo from '../../../resources/consts/contactInfo';
import appStore from './images/appstore-en.svg';
import googlePlay from './images/googleplay-en.svg';
import Logo from './images/logo_mobile.svg';
import path from '../../../resources/consts/path';
import './style.scss';
import { store } from '../../../index';
import langOptions from '../../../resources/consts/langOptions';

import text from './lang.json';

const Footer = props => (
  <div className='footer'>
    <Grid stackable>
      <Grid.Row className='logo'>
        <Grid.Column>
          <Image src={Logo} size='small' className='above-mobile' />
          <div className='below-mobile center-text'>
            <Dropdown
              className='lang-dropdown'
              item
              text={
                <div>
                  <Flag name={props.lang === 'FR' ? 'ca' : 'us'} />
                  {props.lang}
                </div>
              }
            >
              <Dropdown.Menu>
                <Dropdown.Item
                  onClick={() => store.dispatch(changeLang(langOptions.FR))}
                >
                  <Flag name='ca' /> FR
                </Dropdown.Item>
                <Dropdown.Item
                  onClick={() => store.dispatch(changeLang(langOptions.EN))}
                >
                  <Flag name='us' /> EN
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </div>
        </Grid.Column>
      </Grid.Row>

      <Grid.Row centered columns={4}>
        <Grid.Column width={4}>
          <Header>{text[props.lang].title}</Header>
          <div className='sitemap'>
            <ul>
              <li>
                <Link to={path.HOMEPAGE}>
                  {text[props.lang].sitemap.product}
                </Link>
              </li>
              <li>
                <Link to={path.ABOUT}>{text[props.lang].sitemap.about}</Link>
              </li>
              <li>
                <Link to={path.CONTACT}>
                  {text[props.lang].sitemap.contact}
                </Link>
              </li>
              <li>
                <Link to={path.JOBS}>{text[props.lang].sitemap.jobs}</Link>
              </li>
            </ul>
          </div>
        </Grid.Column>
        <Grid.Column width={4} className='contact'>
          <Header>{text[props.lang].contact}</Header>
          <div>{text[props.lang].adress}</div>
          <br />
          <div>{text[props.lang].phone}</div>
          <br />
          <div>{text[props.lang].email}</div>
        </Grid.Column>
        <Grid.Column width={4}>
          <Header>{text[props.lang].download}</Header>
          <a href={contactInfo.PLAYSTORE} rel='noopener noreferrer' target='_blank'>
            <img
              alt='Get it on Google Play'
              className='app-link'
              src={googlePlay}
            />
          </a>
          <a href={contactInfo.APPSTORE} rel='noopener noreferrer' target='_blank'>
            <img
              alt='Available on the App Store'
              className='app-link'
              src={appStore}
            />
          </a>
        </Grid.Column>
        <Grid.Column width={4}>
          <Header>{text[props.lang].follow}</Header>
          <ul>
            <li>
              <a
                target='_blank'
                rel='noopener noreferrer'
                href={contactInfo.FACEBOOK}
              >
                <i className='facebook icon' />
              </a>
            </li>
            <li>
              <a
                target='_blank'
                rel='noopener noreferrer'
                href={contactInfo.INSTAGRAM}
              >
                <i className='instagram icon' />
              </a>
            </li>
            <li>
              <a
                target='_blank'
                rel='noopener noreferrer'
                href={contactInfo.TWITTER}
              >
                <i className='twitter square icon' />
              </a>
            </li>
            <li>
              <a
                target='_blank'
                rel='noopener noreferrer'
                href='https://www.linkedin.com/company/upscale-technology/'
              >
                <i className='linkedin alternate icon' />
              </a>
            </li>
          </ul>
        </Grid.Column>
      </Grid.Row>

      <Grid.Row className='logo'>
        <Grid.Column>
          <div className='cpyright'>© 2018 Upscale Technology Inc.</div>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  </div>
);

Footer.propTypes = {
  lang: PropTypes.string.isRequired
};

export default Footer;
