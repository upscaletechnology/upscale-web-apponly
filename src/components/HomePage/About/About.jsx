import React from 'react';
import { Header, Grid, Container, Image, Reveal } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import './style.scss';

import benoit from './images/benoit/benoit.jpg';
import benoitHappy from './images/benoit/benoitHappy.jpg';
import francis from './images/francis/francis.jpg';
import francisHappy from './images/francis/francisHappy.jpg';
import jimmy from './images/jimmy/jimmy.jpg';
import jimmyHappy from './images/jimmy/jimmyHappy.jpg';
import mikael from './images/mikael/mikael.jpg';
import mikaelHappy from './images/mikael/mikaelHappy.jpg';
import mila from './images/mila/mila.jpg';
import milaHappy from './images/mila/milaHappy.jpg';
import philippe from './images/philippe/philippe.jpg';
import philippeHappy from './images/philippe/philippeHappy.jpg';
import sami from './images/sami/sami.jpg';
import samiHappy from './images/sami/samiHappy.jpg';

import team from './images/team.jpg';
import teamHappy from './images/teamHappy.jpg';

import text from './lang.json';
import drum from './images/drum.jpg';
import centech from './images/partner01.png';
import hec from './images/partner02.png';
import cnrc from './images/partner03.png';
import futurPreneur from './images/partner04.png';

// https://images.unsplash.com/photo-1514286969571-5142af56b991?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=12f250139fca2206e67f482d44afe14a&auto=format&fit=crop&w=1350&q=80
// https://images.unsplash.com/photo-1510590256405-ddf6bda038e4?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=976c92757ccc4bb306e25114b87cebf6&auto=format&fit=crop&w=500&q=60

const TeamMember = props => (
  <Grid.Column className='team-member' >
    <Reveal animated='small fade'>
      <Reveal.Content visible>
        <img className='member-image' src={props.src} alt='dwight' />
      </Reveal.Content>
      <Reveal.Content hidden>
        <img className='member-image' src={props.srcHappy} alt='dwight' />
      </Reveal.Content>
    </Reveal>
    <div className='member-name'>{props.name}</div>
    <div>{props.title}</div>
  </Grid.Column>
);

const About = props => (
  <div className='about' >
    <Container >
      <Header as='h2'>{text[props.lang].header}</Header>
      <Header as='h3'>{text[props.lang].subheader}</Header>
      <hr />
    </Container>
    <Container >
      <Grid stackable >
        <Grid.Column width={6} >
          <Header as='h3' className='section-title' >{text[props.lang].visionTitle}</Header>
          <p className='content' >{text[props.lang].visionContent}</p>
        </Grid.Column>
        <Grid.Column width={10} >
          <div className='drum-img' >
            <Image src={drum} />
          </div>
        </Grid.Column>
      </Grid>
    </Container>
    <Container >
      <Grid reversed='mobile' stackable>
        <Grid.Column width={10} >
          <Image src='https://images.unsplash.com/photo-1493247916314-b6e2d33e25b6?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=8d12a4b2b2e98255ad276e096ac7eb22&auto=format&fit=crop&w=2089&q=80' />
        </Grid.Column>
        <Grid.Column width={6} >
          <Header as='h3' className='section-title' >{text[props.lang].whoTitle}</Header>
          <p className='content' >{text[props.lang].whoContent[0]}</p>
          <p className='content' >{text[props.lang].whoContent[1]}</p>
        </Grid.Column>
      </Grid>
    </Container>
    <Container className='team' >
      <Header as='h2' >{text[props.lang].teamTitle}</Header>
      <hr />
      <Grid columns={3} >
        <Grid.Row>
          <Header className='teamSection' as='h3' >{text[props.lang].founders}</Header>
        </Grid.Row>
        <Grid.Row className='pad-bottom' >
          <TeamMember
            src={francis}
            srcHappy={francisHappy}
            name={text[props.lang].teamMembers[0].name}
            title={text[props.lang].teamMembers[0].title}
          />
          <TeamMember
            src={sami}
            srcHappy={samiHappy}
            name={text[props.lang].teamMembers[1].name}
            title={text[props.lang].teamMembers[1].title}
          />
        </Grid.Row>
        <Grid.Row>
          <Header className='teamSection' as='h3' >{text[props.lang].employees}</Header>
        </Grid.Row>
        <Grid.Row className='pad-bottom' >
          <TeamMember
            src={benoit}
            srcHappy={benoitHappy}
            name={text[props.lang].teamMembers[2].name}
            title={text[props.lang].teamMembers[2].title}
          />
          <TeamMember
            src={jimmy}
            srcHappy={jimmyHappy}
            name={text[props.lang].teamMembers[3].name}
            title={text[props.lang].teamMembers[3].title}
          />
          <TeamMember
            src={mikael}
            srcHappy={mikaelHappy}
            name={text[props.lang].teamMembers[4].name}
            title={text[props.lang].teamMembers[4].title}
          />
          <TeamMember
            src={mila}
            srcHappy={milaHappy}
            name={text[props.lang].teamMembers[5].name}
            title={text[props.lang].teamMembers[5].title}
          />
          <TeamMember
            src={philippe}
            srcHappy={philippeHappy}
            name={text[props.lang].teamMembers[6].name}
            title={text[props.lang].teamMembers[6].title}
          />
        </Grid.Row>
      </Grid>
    </Container>
    <Container>
      <Reveal animated='small fade'>
        <Reveal.Content className='group-wrapper' visible>
          <img className='group-photo' src={team} alt='dwight' />
        </Reveal.Content>
        <Reveal.Content className='group-wrapper' hidden>
          <img className='group-photo' src={teamHappy} alt='dwight' />
        </Reveal.Content>
      </Reveal>
    </Container>
    <Container className='partners' >
      <Header as='h2' >{text[props.lang].partners}</Header>
      <hr />
      <Grid columns={4}>
        <Grid.Column computer={4} tablet={4} largeScreen={4} mobile={6} >
          <Image src={centech} />
        </Grid.Column>
        <Grid.Column computer={4} tablet={4} largeScreen={4} mobile={6}>
          <Image src={hec} />
        </Grid.Column>
        <Grid.Column computer={4} tablet={4} largeScreen={4} mobile={6} >
          <Image src={futurPreneur} />
        </Grid.Column>
        <Grid.Column computer={4} tablet={4} largeScreen={4} mobile={6} >
          <Image src={cnrc} />
        </Grid.Column>
      </Grid>
    </Container>
  </div>
);

TeamMember.propTypes = {
  title: PropTypes.string.isRequired,
  src: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired
};

export default About;
