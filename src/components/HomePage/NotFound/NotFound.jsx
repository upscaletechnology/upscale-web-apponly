import React from 'react';
import { Header, Container } from 'semantic-ui-react';
import PropTypes from 'prop-types';

import './style.scss';

import text from './lang.json';

const NotFound = props => (
  <div className='not-found' >
    <Container>
      <Header as='h2' >{text[props.lang].header}</Header>
      <hr />
      <p>{text[props.lang].subheader}</p>
    </Container>
  </div>
);

NotFound.propTypes = {
  lang: PropTypes.string.isRequired
};


export default NotFound;
