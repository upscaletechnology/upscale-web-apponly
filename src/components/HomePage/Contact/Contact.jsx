import React from 'react';
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from 'react-google-maps';
import { Container, Grid, Header, Button } from 'semantic-ui-react';
import { Field, reduxForm, reset } from 'redux-form';
import PropTypes from 'prop-types';
import text from './lang.json';
import './style.scss';


const MyMapComponent = withScriptjs(withGoogleMap(props => (
  <GoogleMap
    defaultZoom={12}
    defaultCenter={{ lat: 45.496160, lng: -73.561779 }}
  >
    {props.isMarkerShown && <Marker position={{ lat: 45.496160, lng: -73.561779 }} />}
  </GoogleMap>
)));


const Contact = props => (
  <div className='contact' >
    <Container>
      <Header as='h2'>{text[props.lang].header}</Header>
      <Header as='h3'>{text[props.lang].subheader}</Header>
      <hr />
    </Container>
    <Container>
      <Grid stackable >
        <Grid.Column computer={8} tablet={8} largeScreen={8} mobile={12} >
          <form
            action='https://upscale-technology.us17.list-manage.com/subscribe/post?u=9dee5e4da2c3fb6617f9ce97e&amp;id=9dbe55fc9f'
            method='post'
            id='mc-embedded-subscribe-form'
            name='mc-embedded-subscribe-form'
            className='ui form'
            target='_blank'
            noValidate
          >

            <div className='field'>
              <div className='two fields'>

                <div className='field'>
                  <label>{text[props.lang].firstname}</label>
                  <Field name='FNAME' className='firstname' component='input' type='text' />
                </div>

                <div className='field'>
                  <label>{text[props.lang].lastname}</label>
                  <Field name='LNAME' className='lastname' component='input' type='text' />
                </div>

              </div>
            </div>

            <div className='field'>
              <label>{text[props.lang].email}</label>
              <div>
                <Field name='EMAIL' className='form-control' component='input' type='email' />
              </div>
            </div>

            <div className='field'>
              <label>{text[props.lang].message}</label>
              <div>
                <Field name='MMERGE3' className='form-control' component='textarea' type='text' />
              </div>
            </div>

            {/* <div className='g-recaptcha' data-sitekey='6LdRHGMUAAAAAAskMQZGA7DmS-_S7jcYJJ9ZVvS_' /> */}

            <div>
              <Button>{text[props.lang].contactbtn}</Button>
            </div>

          </form>
        </Grid.Column>
        <Grid.Column computer={8} tablet={8} largeScreen={8} mobile={12} >
          <MyMapComponent
            isMarkerShown
            googleMapURL='https://maps.googleapis.com/maps/api/js?key=AIzaSyA9kf3TTp2qm-VncyzNU0Cc_lEGRW2MgKk&v=3.exp&libraries=geometry,drawing,places'
            loadingElement={<div style={{ height: '100%' }} />}
            containerElement={<div style={{ height: '400px' }} />}
            mapElement={<div style={{ height: '100%' }} />}
          />
        </Grid.Column>
      </Grid>
    </Container>
  </div>
);

const afterSubmit = (result, dispatch) => {
  dispatch(reset('contactForm'));
};

Contact.propTypes = {
  lang: PropTypes.string.isRequired
};


export default reduxForm({
  form: 'contactForm',
  onSubmitSuccess: afterSubmit
})(Contact);
