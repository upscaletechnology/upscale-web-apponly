import React from 'react';
import { Menu, Grid, Container, Dropdown, Flag } from 'semantic-ui-react';
import { Element } from 'react-scroll';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import path from '../../../resources/consts/path';
import smoothScroll from '../../../resources/consts/smoothScroll';
import changeLang from '../../../actions/changeLang.action';
import { mobileMenuToggling } from '../../../actions/menu.action';
import { renderMenuItems, renderSecondaryMenuItems } from './menuItems';
import { store } from '../../../index';
import langOptions from '../../../resources/consts/langOptions';
import logo from './images/logo.svg';
import logoMobile from './images/logo_mobile.svg';
import './style.scss';

const soundcheckIcon = require('./images/soundcheck-icon.svg')
  ;

const NavBar = props => (
  <Element name={smoothScroll.TOP}>
    <div className='nav-bar'>
      <Grid>
        <Grid.Column computer={3} className='above-mobile'>
          <Link to={path.HOMEPAGE} className='b-n'>
            <img
              className='logo above-mobile'
              src={logo}
              alt='Upscale Technology'
            />
          </Link>
        </Grid.Column>
        <Grid.Column
          mobile={16}
          computer={13}
          tablet={13}
          largeScreen={13}
          widescreen={13}
        >
          <Container>
            <Grid>
              <Grid.Row>
                <Menu
                  pointing
                  secondary
                  inverted
                  className='right top-menu above-mobile'
                >
                  {renderSecondaryMenuItems(props.activeItem, props.lang)}
                  <Menu.Item>
                    <Dropdown className='lang-dropdown' item text={props.lang}>
                      <Dropdown.Menu>
                        <Dropdown.Item
                          onClick={() =>
                            store.dispatch(changeLang(langOptions.FR))
                          }
                        >
                          <Flag name='ca' /> FR
                        </Dropdown.Item>
                        <Dropdown.Item
                          onClick={() =>
                            store.dispatch(changeLang(langOptions.EN))
                          }
                        >
                          <Flag name='us' /> EN
                        </Dropdown.Item>
                      </Dropdown.Menu>
                    </Dropdown>
                  </Menu.Item>
                </Menu>
              </Grid.Row>
              <Grid.Row>
                <Link to={path.HOMEPAGE} className='logo-mobile below-mobile'>
                  <img
                    className='below-mobile'
                    src={logoMobile}
                    alt='Upscale Technology'
                  />
                </Link>
                <Menu
                  pointing
                  secondary
                  inverted
                  style={{ position: 'relative' }}
                  className='right main-menu above-mobile'
                  onClick={() => store.dispatch(mobileMenuToggling())}
                >
                  {renderMenuItems(false, props.activeItem, props.lang)}
                </Menu>
                <a className='soundcheck-link' href='https://app.upscale-technology.com'>
                  <div className='soundcheck-button'
                  >
                    <img src={soundcheckIcon} />
                    <p style={{ fontSize: '22px', fontWeight: 'bold' }}>
                      Soundcheck
                    </p>
                  </div>
                </a>
              </Grid.Row>
            </Grid>
          </Container>
        </Grid.Column>
      </Grid>
    </div>
  </Element>
);

NavBar.propTypes = {
  activeItem: PropTypes.string.isRequired,
  toggle: PropTypes.bool,
  lang: PropTypes.string.isRequired
};

NavBar.defaultProps = {
  toggle: false
};

export default NavBar;
