import React from 'react';
import { Menu } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import path from '../../../resources/consts/path';
import { store } from '../../../index';
import { changeMenuItem } from '../../../actions/menu.action';

import text from './lang.json';

export const menuItems = [
  { name: 'product', link: path.HOMEPAGE },
  { name: 'about', link: path.ABOUT },
  { name: 'contact', link: path.CONTACT }
];

export const secondaryMenuItems = [ { name: 'jobs', link: path.JOBS }, { name: 'blog', link: 'https://medium.com/@upscaletechnology' } ];

export const renderMainItems = (mobile, state, menuItem, lang) => (
  <Menu.Item
    as={Link}
    to={menuItem.link}
    key={menuItem.name}
    name={text[lang].mainMenu[menuItem.name]}
    content={text[lang].mainMenu[menuItem.name]}
    className={mobile ? 'below-mobile' : 'above-mobile common'}
    active={state === menuItem.name}
    onClick={() => store.dispatch(changeMenuItem(menuItem.name))}
  />
);

export const renderSecondaryItems = (state, menuItem, lang) => (
  menuItem.name === 'blog' ?
    <Menu.Item
      href={menuItem.link}
      target='_blank'
      to={menuItem.link}
      key={menuItem.name}
      name={text[lang].secondaryMenu[menuItem.name]}
      active={state === menuItem.name}
      onClick={() => store.dispatch(changeMenuItem(menuItem.name))}
    /> : <Menu.Item
      as={Link}
      to={menuItem.link}
      key={menuItem.name}
      name={text[lang].secondaryMenu[menuItem.name]}
      active={state === menuItem.name}
      onClick={() => store.dispatch(changeMenuItem(menuItem.name))}
    />
);

export const renderMobileNav = lang =>
  menuItems.map(menuItem => (
    <Link to={menuItem.link}>{text[lang].mainMenu[menuItem.name]}</Link>
  ));

export const renderMenuItems = (mobile, state, lang) =>
  menuItems.map(menuItem => renderMainItems(mobile, state, menuItem, lang));

export const renderSecondaryMenuItems = (state, lang) =>
  secondaryMenuItems.map(menuItem =>
    renderSecondaryItems(state, menuItem, lang),);
