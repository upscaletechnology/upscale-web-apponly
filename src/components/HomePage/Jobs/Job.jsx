import React from 'react';
import { Header, Container } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import NotFound from '../NotFound/NotFound';
import text from './lang.json';


const Job = (props) => {
  let jobOffer = null;
  const found = text[props.lang].jobs.find((job) => {
    if (job.path === props.match.params.jobtitle) {
      jobOffer = job;
      return job.path === props.match.params.jobtitle;
    }
  });
  if (!found) {
    return <NotFound lang={props.lang} />;
  }
  return (
    <div className='job' >
      <Container>
        <Header as='h2'>{jobOffer.title}</Header>
        <div >{jobOffer.description}</div>
      </Container>
      <Container>
        <div className='title'>{text[props.lang].taskTitle}</div>
        <ul>
          {jobOffer.tasks.map(task => <li>{task}</li>)}
        </ul>
      </Container>
      <Container>
        <div className='title' >{text[props.lang].skillsTitle}</div>
        <ul>
          {jobOffer.skills.map(skill => <li>{skill}</li>)}
        </ul>
      </Container>
      <Container>
        <div>{text[props.lang].interest}</div>
      </Container>
    </div>
  );
};

Job.propTypes = {
  match: PropTypes.object.isRequired,
  lang: PropTypes.string.isRequired
};


export default Job;
