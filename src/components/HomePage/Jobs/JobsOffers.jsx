import React from 'react';
import { Grid, Header, Container } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import text from './lang.json';

const RenderJobsOffers = props => text[props.lang].jobs.map(job => (
  <Grid.Column>
    <div className='jobOffer'>
      <div className='title' >
        <Link to={`${props.match.path}/${job.path}`}>{job.title}</Link>
      </div>
      <div className='content'>{job.description}</div>
    </div>
  </Grid.Column>
));

const JobsOffers = props => (
  <div className='jobs' >
    <Container>
      <Header as='h2'>{text[props.lang].header}</Header>
      <Header as='h3'>{text[props.lang].subheader}</Header>
      <p>{text[props.lang].motivation}</p>
      <hr />
    </Container>
    <Container>
      <Grid columns={3} stackable >
        <Grid.Row centered >
          <Header as='h3' className='section-title'>{text[props.lang].role}</Header>
        </Grid.Row>
        <RenderJobsOffers match={props.match} lang={props.lang} />
      </Grid>
    </Container>
  </div>
);

JobsOffers.propTypes = {
  match: PropTypes.object.isRequired,
  lang: PropTypes.object.isRequired
};


export default JobsOffers;
