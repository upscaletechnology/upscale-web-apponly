import React from 'react';
import { Switch, Route } from 'react-router-dom';
import PropTypes from 'prop-types';
import Job from './Job';
import JobsOffers from './JobsOffers';

import './style.scss';

// import path from '../../../resources/consts/path';

const Jobs = props => (
  <div>
    <Switch>
      <Route exact path={`${props.match.path}`} component={() => <JobsOffers match={props.match} lang={props.lang} />} />
      <Route path={`${props.match.path}/:jobtitle`} component={({ match }) => <Job match={match} lang={props.lang} />} />
    </Switch>
  </div>
);

Jobs.propTypes = {
  match: PropTypes.object.isRequired,
  lang: PropTypes.string.isRequired
};


export default Jobs;
