import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import menu from './menu';
import lang from './lang';

const rootReducer = combineReducers({
  menu,
  lang,
  form: formReducer
});

export default rootReducer;
