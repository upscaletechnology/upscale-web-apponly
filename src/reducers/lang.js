import actionsTypes from '../resources/consts/actionsTypes';

const stateInit = () => {
  let lang = navigator.language;
  while (lang.length > 2) {
    lang = lang.slice(0, -1);
  }
  return lang.toUpperCase();
};

const lang = (state = stateInit(), action) => {
  switch (action.type) {
    case actionsTypes.CHANGELANG:
      return action.payload;

    default:
      return state;
  }
};

export default lang;
