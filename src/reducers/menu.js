import actionsTypes from '../resources/consts/actionsTypes';

const initState = {
  toggle: false,
  activeItem: ''
};

const menu = (state = initState, action) => {
  switch (action.type) {
    case actionsTypes.MENUTOGGLE:
      return {
        ...state,
        toggle: !state.toggle
      };

    case actionsTypes.CHANGEURL:
      return {
        ...state,
        activeItem: action.payload
      };

    default:
      return state;
  }
};

export default menu;
