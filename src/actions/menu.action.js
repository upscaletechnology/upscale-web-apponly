import actionsTypes from '../resources/consts/actionsTypes';


export const mobileMenuToggling = () => ({
  type: actionsTypes.MENUTOGGLE
});

export const changeMenuItem = menuItem => ({
  type: actionsTypes.CHANGEURL,
  payload: menuItem
});
