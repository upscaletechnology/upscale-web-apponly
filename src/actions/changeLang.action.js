import actionsTypes from '../resources/consts/actionsTypes';


const changeLang = lang => ({
  type: actionsTypes.CHANGELANG,
  payload: lang
});

export default changeLang;
